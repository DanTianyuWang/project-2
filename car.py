'''
Project 2 
Traffic problem
Car class
'''

class Car():
	def __init__(self, pos = 0, vel = 1):
		self.pos = pos
		self.vel = vel
		self.speed_lim = 1 # We could just write this number in, but this
						 # allows us to change it if we want to later
						 

	def update_pos(self, dt=1):
		self.pos += self.vel * dt


	def move(self, car_ahead, dt = 1):
		# IMPORTANT: This function is built under the assumption that you iterate
		# from the "Newest" car on the road to the "Oldest"
		# Checks position of car in front. 
		#    If a collision will occur, Decelerate (condition is that pos1 will be > pos2)
		#    If no collision will occur and there's room, Accelerate (condition is that pos1 will be < pos2)
		if car_ahead == None:
			self.vel = 1
		elif self.pos + self.vel*dt > car_ahead.pos:
			# Decrease vel to be 2/3 the position gap
			self.vel = 2.0*(car_ahead.pos - self.pos)/3.0
		elif self.pos + self.vel*dt < car_ahead.pos:
			# Increase vel to be 2/3 the gap between pos + vel*dt and car_ahead.pos
			self.vel += 2.0*(car_ahead.pos - (self.pos + self.vel * dt))/3.0
			if self.vel > self.speed_lim:
				self.vel = self.speed_lim # I'm not allowed to go above the speed limit :)

		self.update_pos(dt)




if __name__ == '__main__':
	a = Car(10, 0.5)
	b = Car(9.5, 1)
	c = Car(9, 1)
	d = Car(8, 1)
	cars = [d, c, b, a, None] # list of cars
	cars_pos = []
	for i in range(0, len(cars)-1):
		cars_pos += [cars[i].pos]
	time = 0
	while time < 20:
		# updating positions of cars
		for i in range(0, len(cars) - 1):
			cars_pos[i] = cars[i].pos
			cars[i].move(cars[i+1])
		print cars_pos
		time += 1

